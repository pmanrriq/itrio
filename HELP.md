# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Spring initializr](https://start.spring.io/)

Esta Url de Spring la uso para crear un proyecto de zero, despues podes configurarlo mejor haciendo algunas modificaciones al proyecto

### Estructura del proyecto
- application
    
    Aqui van las reglas de negocio, (Casos de uso) la particularidad es que no tiene que estar acoplado al framework.
  (Las clases no pueden estar @)
- configuration
    Aqui van las clases de configuracion hay algunas de ejmplo, ademas se pueden ir clases properties, etc
- controller
    Aqui van los controladores, un controlador debe llamar a un caso de uso, clase definida en la carpeta application

- infrastructure
    Aqui van los repositorios, los gateway, etc, clases externas o que conectan a otra api etc 
- model 
    Aqui van las entidades, los dto, los response, las enumeraciones, todo lo que sea un data class
