CREATE TABLE IF NOT EXISTS itrioDb.PERSON
(
	per_id		int NOT NULL auto_increment,
    per_name  varchar(256) not null,
    per_last_name varchar(256) not null,
    per_dni varchar(256) not null,
    per_birthday date,
    per_create_time datetime,
    primary key (per_id)
);

create index idx_per_dni on itrioDb.PERSON(per_dni);