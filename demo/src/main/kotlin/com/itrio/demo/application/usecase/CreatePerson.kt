package com.itrio.demo.application.usecase

import com.itrio.demo.infrastructure.repository.PersonRepository
import com.itrio.demo.model.entity.Person
import io.swagger.v3.core.util.Json
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import reactor.core.publisher.Mono
import java.time.LocalDate
import java.time.LocalDateTime

class CreatePerson(
    val personRepository: PersonRepository,
    val log: Logger = LoggerFactory.getLogger(CreatePerson::class.java)
) {

    fun invoke(
        name: String,
        lastName: String,
        dni: String,
        birthday: LocalDate
    ): Mono<Person> {
        var person = Person(
            name = name,
            lastName = lastName,
            dni = dni,
            birthday = birthday,
            createTime = LocalDateTime.now()
        )

        return personRepository.save(person)
            .doOnSuccess{
                log.info("[NEW PERSON] ${Json.pretty(it)}")
            }
    }

}