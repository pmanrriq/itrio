package com.itrio.demo.configuration.observability

import reactor.netty.http.server.logging.AccessLogArgProvider

class MessageAccessLog(
    accessLogArgProvider: AccessLogArgProvider,
    env: String
) {
    val observability = ObservabilityMessageAccessLog(accessLogArgProvider)
    val envairoment = env
    val http = HttpMessagesAccessLog(
        accessLogArgProvider.method(),
        accessLogArgProvider.uri(),
        accessLogArgProvider.status()?.toString(),
        accessLogArgProvider.duration()
    )
}