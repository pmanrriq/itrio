package com.itrio.demo.configuration

import com.itrio.demo.configuration.observability.FilterAccessLog
import com.itrio.demo.configuration.observability.MessageAccessLog
import io.swagger.v3.core.util.Json
import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory
import org.springframework.boot.web.embedded.netty.NettyServerCustomizer
import org.springframework.boot.web.server.WebServerFactoryCustomizer
import org.springframework.context.annotation.Configuration
import reactor.netty.http.server.HttpServer
import reactor.netty.http.server.logging.AccessLog
import reactor.netty.http.server.logging.AccessLogFactory


@Configuration
class NettyHandlerDelete() : WebServerFactoryCustomizer<NettyReactiveWebServerFactory> {

    override
    fun customize(factory: NettyReactiveWebServerFactory?) {
        factory!!.addServerCustomizers(
            NettyServerCustomizer { httpServer: HttpServer ->

                httpServer.accessLog(
                    true,
                    AccessLogFactory.createFilter(
                        { FilterAccessLog.isValidUri(it.uri()) },
                        { AccessLog.create(Json.pretty(MessageAccessLog(it, "prd"))) }
                    )
                )


            }
        )
    }
}