package com.itrio.demo.configuration.observability

import reactor.netty.http.server.logging.AccessLogArgProvider

class ObservabilityMessageAccessLog(
    accessLogArgProvider: AccessLogArgProvider
) {
    val consumer_id = accessLogArgProvider.requestHeader("consumer-id").toString()
    val flow_id = accessLogArgProvider.requestHeader("flow-id").toString()
    val owner = "cuentas-transaccional"
    val vertical = "financial-development"
    val trace_id = accessLogArgProvider.requestHeader("trace-id").toString()
}