package com.itrio.demo.configuration

import io.netty.handler.logging.LogLevel
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient
import reactor.netty.transport.logging.AdvancedByteBufFormat

@Configuration
class WebClientConfiguration {

    @Bean
    fun getWebClient(): WebClient {
        return WebClient
            .builder()
            .clientConnector(
                ReactorClientHttpConnector(
                    HttpClient.create()
                        .wiretap(
                            "reactor.netty.http.client.HttpClient", LogLevel.INFO,
                            AdvancedByteBufFormat.TEXTUAL
                        )
                        .compress(false)
                )
            )
            .build()
    }
}