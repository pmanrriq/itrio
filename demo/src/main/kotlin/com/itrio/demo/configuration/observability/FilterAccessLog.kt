package com.itrio.demo.configuration.observability

class FilterAccessLog {

    companion object {
        private val accessLogUriExceptions = listOf("actuator")

        fun isValidUri(uri: CharSequence?): Boolean {
            return accessLogUriExceptions.any { uriException -> uri.let { uriException !in it.toString() }  }

        }

    }
}