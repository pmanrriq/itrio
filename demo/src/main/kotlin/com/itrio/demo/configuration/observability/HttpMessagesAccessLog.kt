package com.itrio.demo.configuration.observability

data class HttpMessagesAccessLog(val method: CharSequence?, val uri: CharSequence?, val statusCode: String?, val elapsedTime: Long)