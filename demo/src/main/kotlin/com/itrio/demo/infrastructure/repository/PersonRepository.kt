package com.itrio.demo.infrastructure.repository

import com.itrio.demo.model.entity.Person
import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface PersonRepository: ReactiveCrudRepository<Person, Int> {
}