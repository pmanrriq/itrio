package com.itrio.demo.model.request

import com.fasterxml.jackson.annotation.JsonInclude
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Pattern

import java.time.LocalDate

@JsonInclude(JsonInclude.Include.NON_NULL)
data class PersonRequest(
    @field:NotNull(message = "Name must not be null")
    var name: String?,
    @field:NotNull(message = "Last Name must not be null")
    var lastName: String?,
    @field:NotNull(message = "dni must not be null")
    var dni: String?,
    var birthday: LocalDate
)