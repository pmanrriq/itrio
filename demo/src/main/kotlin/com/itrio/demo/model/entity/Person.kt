package com.itrio.demo.model.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDate
import java.time.LocalDateTime

@Table(value = "PERSON")
data class Person(
    @Id
    @Column(value = "per_id")
    var id: Int? = null,
    @Column(value = "per_name")
    var name: String? = null,
    @Column(value = "per_last_name")
    var lastName: String? = null,
    @Column(value = "per_dni")
    var dni: String? = null,
    @Column(value = "per_birthday")
    var birthday: LocalDate? = null,
    @Column(value = "per_create_time")
    var createTime: LocalDateTime? = null
)