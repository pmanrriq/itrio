package com.itrio.demo.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
@RequestMapping
class HelloWorld(
    private val log: Logger = LoggerFactory.getLogger(HelloWorld::class.java)
) {

    @GetMapping("/hello")
    fun invoke() : Mono<ResponseEntity<String>> {
        log.info("Hello World")
        return Mono.just("Hello World")
            .map { ResponseEntity.ok("Hello World") }

    }
}