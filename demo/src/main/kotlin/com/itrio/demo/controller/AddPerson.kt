package com.itrio.demo.controller

import com.itrio.demo.application.usecase.CreatePerson
import com.itrio.demo.infrastructure.repository.PersonRepository
import com.itrio.demo.model.entity.Person
import com.itrio.demo.model.request.PersonRequest
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.net.URI
import java.time.LocalDate

@RestController
@RequestMapping("/itrio")
@Validated
class AddPerson(
    personRepository: PersonRepository
) {
    private val createPerson: CreatePerson = CreatePerson(personRepository)

    @PostMapping("/person")
    fun invoke(
        @Valid @RequestBody body: PersonRequest
    ): Mono<ResponseEntity<Person>> {
        return createPerson.invoke(
            body.name!!,
            body.lastName!!,
            body.dni!!,
            body.birthday
        ) .map {
            ResponseEntity.created(URI.create("/itrio/person"))
            .body(it)
        }
    }
}